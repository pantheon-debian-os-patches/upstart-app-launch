upstart-app-launch (0.3+14.04.20140411-0+pantheon1) jessie; urgency=medium

  * Backport from ubuntu. 

 -- Nicolas Bruguier <gandalfn@club-internet.fr>  Mon, 29 Sep 2014 20:42:57 +0200

upstart-app-launch (0.3+14.04.20140411-0ubuntu1) trusty; urgency=low

  [ Ted Gould ]
  * Kill all jobs in process group on exit

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Fri, 11 Apr 2014 22:14:55 +0000

upstart-app-launch (0.3+14.04.20140321-0ubuntu1) trusty; urgency=low

  [ Colin Watson ]
  * Use libclick to get the package directory, saving about 0.7 seconds
    from Click application startup (on mako).
  * Use libclick to get package manifests, saving about 0.7 seconds from
    Click application startup (on mako).

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Fri, 21 Mar 2014 17:56:24 +0000

upstart-app-launch (0.3+14.04.20140320-0ubuntu1) trusty; urgency=low

  [ Thomi Richards ]
  * Export the correct environment variable to load apps with
    testability enabled. (LP: #1285947)

  [ Ubuntu daily release ]
  * New rebuild forced

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 20 Mar 2014 23:22:49 +0000

upstart-app-launch (0.3+14.04.20140220-0ubuntu3) trusty; urgency=medium

  * Change dependency on "zeitgeist" to "zeitgeist-core". The former is a
    metapackage which pulls in python2 and useless gtk/X data providers,
    whereas zeitgeist-core provides the service necessary for
    zg-report-app to operate.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Sat, 22 Feb 2014 23:00:37 +0000

upstart-app-launch (0.3+14.04.20140220-0ubuntu2) trusty; urgency=medium

  * Drop depends:click from a Multi-Arch:same library package, as click is
    neither M-A:allowed nor M-A:foreign. This unbreaks cross-compilation.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Thu, 20 Feb 2014 23:51:29 +0000

upstart-app-launch (0.3+14.04.20140220-0ubuntu1) trusty; urgency=low

  [ Ted Gould ]
  * Support for Untrusted Helpers
  * A little tool to discover application IDs
  * Add observers in the launch tool so that we stick around to handle
    events
  * Add functions for testing and App ID parsing

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 20 Feb 2014 12:04:30 +0000

upstart-app-launch (0.3+14.04.20140213-0ubuntu1) trusty; urgency=low

  [ Ted Gould ]
  * Unregister for Unity's Resume signal
  * Function to get the log file for the application
  * Add GIR support to libual
  * Remove an invalid substitution that we weren't using (LP: #1220591)

  [ Ubuntu daily release ]
  * debian/*symbols: auto-update new symbols to released version

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 20 Feb 2014 12:04:27 +0000

upstart-app-launch (0.3+14.04.20140210-0ubuntu1) trusty; urgency=low

  * 
  * debian/*symbols: auto-update new symbols to released version

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 13 Feb 2014 13:14:48 +0000

upstart-app-launch (0.3+14.04.20140206-0ubuntu1) trusty; urgency=low

  [ Ted Gould ]
  * Basic merge and review requirements.
  * On error print the exec line
  * Make the last environment variable set synchronous to make Upstart
    respond that it received it. (LP: #1275017)

  [ Ubuntu daily release ]
  * New rebuild forced

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 06 Feb 2014 16:54:55 +0000

upstart-app-launch (0.3+14.04.20140129.1-0ubuntu1) trusty; urgency=low

  [ Ken VanDine ]
  * Added upstart_app_launch_triplet_to_app_id to construct an appid
    from pkg, app, version triplet. If a specific version isn't
    provided, click is used to determine the version from the manifest.

  [ Ted Gould ]
  * Set the Upstart job environment using DBus.
  * Tests for the exec utilities.
  * Add an APP_DESKTOP_FILE_PATH to point to a readable desktop file
    under confinement.
  * Include an architecture specific directory in the path.
  * Making the application job a task.
  * Add application list handling to the triplet function.

  [ CI bot ]
  * Upload to trusty

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 29 Jan 2014 20:08:11 +0000

upstart-app-launch (0.3+14.04.20131218-0ubuntu1) trusty; urgency=low

  [ Ken VanDine ]
  * Multi-Arch: same libraries must not depend on run-time application,.
    since this breaks cross-compilation. Furthermore, circular
    dependencies are never needed: upstart-app-launch depends on
    libupstart-app-launch2, which depends on upstart-app-launch. It's
    the same reasoning why we do not make libgtk-3.0 depend on epiphany
    webbrowser, even though gtk has functions to open URL in a
    webbrowser. And in a multi-arch world libgtk-3.0 would never know
    which architecture your web-browser is, it could be any. Libraries
    should only ever depend on other linked shared libraries.

  [ Ted Gould ]
  * Make sure that 'package' sticks around until we setup the
    environment. (LP: #1260079)
  * Improve readability of the coverage report.

  [ Didier Roche ]
  * revert the archictecture specific list: britney doesn't use those

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 100
  * debian/*symbols: auto-update new symbols to released version

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 29 Jan 2014 20:08:07 +0000

upstart-app-launch (0.3+14.04.20131209-0ubuntu3) trusty; urgency=low

  * Multi-Arch: same libraries must not depend on run-time application,
    since this breaks cross-compilation. Furthermore, circular
    dependencies are never needed: upstart-app-launch depends on
    libupstart-app-launch2, which depends on upstart-app-launch. It's the
    same reasoning why we do not make libgtk-3.0 depend on epiphany
    webbrowser, even though gtk has functions to open URL in a
    webbrowser. And in a multi-arch world libgtk-3.0 would never know
    which architecture your web-browser is, it could be any. Libraries
    should only ever depend on other linked shared libraries.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Thu, 12 Dec 2013 22:32:34 +0000

upstart-app-launch (0.3+14.04.20131209-0ubuntu2) trusty; urgency=low

  * Restrict on which arch we build url-dispatcher due to the ust build-dep

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 09 Dec 2013 13:47:51 +0100

upstart-app-launch (0.3+14.04.20131209-0ubuntu1) trusty; urgency=low

  [ Ubuntu daily release ]
  * debian/*symbols: auto-update new symbols to released version

  [ Ted Gould ]
  * Use the parsing of the line to split appart the URLs. (LP: #1253703)
  * Use Upstart on the session bus instead of the private bus.
  * Adding tracing support to application startup.
  * Hide click error printed in the Upstart log.
  * Handshake for starting applications. (LP: #1243665)
  * Link to the right version of upstart-app-launch.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 93

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Mon, 09 Dec 2013 02:05:27 +0000

upstart-app-launch (0.3+14.04.20131126-0ubuntu1) trusty; urgency=low

  [ Ted Gould ]
  * Bumping to 0.3 to reflect that we added a new env var to the library
    which is needed for testing URL Dispatcher
  * Reenable ZG and port to libzg-2.0. (LP: #1197569)
  * Ensure quoted single URIs passed to a %U are unquoted. (LP:
    #1253703)

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 86

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 26 Nov 2013 02:07:13 +0000

upstart-app-launch (0.2+14.04.20131119-0ubuntu1) trusty; urgency=low

  [ Ted Gould ]
  * Unref the proxy after stopping an application.
  * Use shell escaping to ensure we can handle spaces. (LP: #1229354)
  * Set the XDG Data Dirs to include the application install directory.
    (LP: #1250546)

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 82

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 19 Nov 2013 08:19:25 +0000

upstart-app-launch (0.2+13.10.20131014-0ubuntu1) saucy; urgency=low

  [ Ricardo Mendoza ]
  * Currently we are signalling observers that app started on the
    "starting" signal. This is racy, as the final exec might not have
    happened yet, so if a client tries to get the primary PID before the
    process has fully started, it might get one of the transitional PIDs
    of the scripts that run in between.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 78

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Mon, 14 Oct 2013 07:55:55 +0000

upstart-app-launch (0.2+13.10.20131011-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Bumping to version 0.2
  * Adding a libupstart-app-launch1.symbols file to match
  * Use 'click info' to get manifests. (LP: #1232118)

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 76

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Fri, 11 Oct 2013 02:09:14 +0000

upstart-app-launch (0.1+13.10.20131008.1-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * On second activations send a message to the FD.o application
    interface. (LP: #1228345)
  * Add observers for focused and resume.
  * Testing of the second exec logic.
  * Disable ZG logging for Phone 1.0.
  * Need a little longer time on recent ARM builds.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 73

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 08 Oct 2013 10:07:25 +0000

upstart-app-launch (0.1+13.10.20130925.2-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Ensure a failed stop return doesn't block execution. (LP: #1229468)

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 67

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 25 Sep 2013 18:03:51 +0000

upstart-app-launch (0.1+13.10.20130925.1-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Tests for the functions in the core helpers library.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 65

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 25 Sep 2013 12:54:36 +0000

upstart-app-launch (0.1+13.10.20130924.1-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Evil hack to allow "secondary activations" through killing the first
    instance.
  * Handle URLs with complex spacing by using C parsing. (LP: #1228387)

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 63

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 24 Sep 2013 06:04:42 +0000

upstart-app-launch (0.1+13.10.20130919.4-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Support a desktop file key so legacy applications can be single
    instance.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 60

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 19 Sep 2013 22:04:58 +0000

upstart-app-launch (0.1+13.10.20130919.3-0ubuntu1) saucy; urgency=low

  [ Loïc Minier ]
  * Fix LP #1227632 by directly joining the components of the Exec line
    split on % instead of joining them with spaces. (LP: #1227632)

  [ Ted Gould ]
  * Only split the URIs if there are more than one.
  * Try to deduce if we're on Surface Flinger.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 58

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 19 Sep 2013 14:03:55 +0000

upstart-app-launch (0.1+13.10.20130917.1-0ubuntu1) saucy; urgency=low

  [ Sebastien Bacher ]
  * typo fix in one of the warning strings.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 54

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 17 Sep 2013 22:03:45 +0000

upstart-app-launch (0.1+13.10.20130912-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Don't automatically warn on a failed App ID.
  * Check to see if an icon exists, and if so prepend the full path.

  [ Jamie Strandboge ]
  * application-legacy.conf.in: use aa-exec-click instead of aa-exec
    desktop-hook.c: use aa-exec-click instead of aa-exec (LP: #1197047)
    debian/control: Depends on click-apparmor. (LP: #1197047)

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 52

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 12 Sep 2013 20:33:42 +0000

upstart-app-launch (0.1+13.10.20130905-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Making sure to look for the application directory. (LP: #1215478)
  * Handle click errors by just not setting the value.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 48

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 05 Sep 2013 09:06:43 +0000

upstart-app-launch (0.1+13.10.20130903-0ubuntu1) saucy; urgency=low

  [ Guenter Schwann ]
  * Make libupstart-app-launch usable by C++ programs.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 45

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 03 Sep 2013 18:05:38 +0000

upstart-app-launch (0.1+13.10.20130827-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Completes the base functionality in the libupstart-app-launch
    library.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 43

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 27 Aug 2013 14:04:33 +0000

upstart-app-launch (0.1+13.10.20130819-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Make setting the PATH more robust.

  [ jdstrand ]
  * Update click-exec.c to setup the sandbox environment. Specifically:
    - explicitly set the XDG base dirs to the user's preference - set
    UBUNTU_APPLICATION_ISOLATION - set TMPDIR - set
    __GL_SHADER_DISK_CACHE_PATH.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 41

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Mon, 19 Aug 2013 02:04:19 +0000

upstart-app-launch (0.1+13.10.20130812-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Use the click utility to determine path to the package.
  * Split profile and exec into two variables.
  * Add a click package hook to build desktop files.
  * Flesh out the click package execution.
  * Switch to CMake and add stub library.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 38

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Mon, 12 Aug 2013 18:04:33 +0000

upstart-app-launch (0.1+13.10.20130712-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Add support for URIs.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 32

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Fri, 12 Jul 2013 00:01:50 +0000

upstart-app-launch (0.1+13.10.20130703-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Split out legacy and click package startup.
  * Allow setting the apparmor profile in the dekstop file.
  * Add Zeitgeist logging of the start and stop of the applications.
  * Adding a shell script to insert the initctl path if on buildd's.
  * Switch initialization to be signal based.
  * Don't run tests on buildds.
  * Changing the script to check for upstart versions.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 30

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 03 Jul 2013 00:01:31 +0000

upstart-app-launch (0.1daily13.06.19-0ubuntu1) saucy; urgency=low

  [ Ted Gould ]
  * Initial release 
  * The application config file
  * Adding debian stuff
  * Fixing the make file to make it work-ish
  * Adding notification of isolation
  * Adding a build for a small executor target
  * Fleshing out desktop exec
  * Adding the directory into the conf file
  * Make the deb stuff work
  * Now that we have the tool we need to expect a fork
  * Changing the conf file to not need the script suggested by zyga on
    #ubuntu-devel
  * Switch from using GIO to standard GLib and exec
  * Simpler pkgconfig too
  * Drop the forking
  * Making a little script to help with apps
  * Putting some boiler plate dbus in there
  * Dump out the PIDs and the nice names
  * Putting the type in too
  * Close the metal here
  * Adding a glib build dependency.
  * Searching desktop files in pre-start.
  * Adding a COPYING file and file copyrights.

  [ Ubuntu daily release ]
  * Automatic snapshot from revision 22

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 19 Jun 2013 00:01:09 +0000
